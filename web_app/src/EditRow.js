import React from 'react';
import PropTypes from 'prop-types';

class EditRow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nameVal: this.props.user.name,
            loginVal: this.props.user.login,
        };

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }


    SaveEdit() {
        var user = {name: this.state.nameVal, login: this.state.loginVal};
        this.props.onClick("update", this.props.user.id, user);
    }

    StornoEdit() {
        this.props.onClick("storno", this.props.user.id, null);
    }

    render() {
        return (
            <tr>
                <td>{this.props.user.id}</td>
                <td><input
                    name="nameVal"
                    type="text"
                    defaultValue={this.props.user.name}
                    onChange={this.handleInputChange} /></td>
                <td><input
                    name="loginVal"
                    type="text"
                    defaultValue={this.props.user.login}
                    onChange={this.handleInputChange} /></td>
                <td>
                    <button onClick={() => this.SaveEdit()}>Save</button>
                </td><td>
                    <button onClick={() => this.StornoEdit()}>X</button>

                </td>
            </tr>
        )
    }
}

export default EditRow