import React from 'react';
import PropTypes from 'prop-types';

function StaticRow(props) {
    const user = props.user;
    return (
        <tr>
            <td>{user.id}</td>
            <td>{user.name}</td>
            <td>{user.login}</td>
            <td>
                <button onClick={() => props.onClick("edit", user.id, null)}>Edit</button>
            </td><td>
                <button onClick={() => props.onClick("del", user.id, null)}>Del</button>
            </td>
        </tr>
    )

}


export default StaticRow