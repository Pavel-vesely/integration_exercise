import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import './index.css';
import MyTable from './MyTable'

//Test data, TODO replace with actual backend
const names = ['Jiří', 'Jan', 'Petr', 'Jana', 'Marie', 'Josef', 'Pavel', 'Martin', 'Tomáš', 'Jaroslav'];
const defaultUsers = names.map((name, index) => ({id: index, name: name, login: name.toLowerCase(), editing: false}));

class Main extends React.Component {
    constructor(props) {
        super(props);
        const loadedUsers = defaultUsers;
        this.state = {
            users: loadedUsers,
            maxId: Math.max.apply(Math, loadedUsers.map(function(user) { return user.id; }))
        }
    }

    insertUser(data) {
        var newArray = this.state.users.slice();
        newArray.push({id: this.state.maxId + 1, name: data.name, login: data.login, editing: false});
        this.setState({users: newArray, maxId: this.state.maxId + 1});
    }

    updateUser(id, data) {
        this.setState({users: this.state.users.filter(function(user) {
                if (user.id === id) {
                    user.name = data.name;
                    user.login = data.login;
                    user.editing = false;
                }
                return user;
            })});
    }

    removeUser(id) {
        this.setState({users: this.state.users.filter(function(user) {
                return user.id === id ? null : user
            })});
    }

    handleClick(type, id, data) {
        if (type === "add") {
            this.insertUser(data);
        } else if (type === "update") {
            this.updateUser(id, data);
        } else if (type === "del") {
            this.removeUser(id);
        } else if (type === "edit") {
            // set editing = true for user by id
            this.setState({users: this.state.users.filter(function(user) {
                    if (user.id === id) {
                        user.editing = true;
                    }
                    return user;
                })});
        } else if (type === "storno") {
            // set editing = false for user by id
            this.setState({users: this.state.users.filter(function(user) {
                    if (user.id === id) {
                        user.editing = false;
                    }
                    return user;
                })});
        }
    }

    render() {
        return (
            <div>
                <MyTable
                    users={this.state.users}
                    onClick={(type, id, data) => this.handleClick(type, id, data)}
                />
            </div>
        )
    }
}

ReactDOM.render(
    <Main />,
    document.getElementById('root')
);