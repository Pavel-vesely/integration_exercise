import React from 'react';
import PropTypes from 'prop-types';
import MyTableBody from './MyTableBody.js'
import MyTableHeader from './MyTableHeader.js'
import NewUserRow from './NewUserRow'

function MyTable(props) {

    return (
        <table>
            <MyTableHeader/>
            <MyTableBody
                users={props.users}
                onClick={(type, id, data) => props.onClick(type, id, data)}
            />
            <NewUserRow
                onClick={(type, id, data) => props.onClick(type, id, data)}
            />
        </table>
    )
}

export default MyTable