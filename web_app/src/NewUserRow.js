import React from 'react';
import PropTypes from 'prop-types';

class NewUserRow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nameVal: '',
            loginVal: '',
        };

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }


    addNewUser() {
        var user = {name: this.state.nameVal, login: this.state.loginVal};
        this.props.onClick("add", -1, user);
    }

    render() {
        return (
            <tr>
                <td></td>
                <td><input
                    name="nameVal"
                    type="text"
                    onChange={this.handleInputChange} /></td>
                <td><input
                    name="loginVal"
                    type="text"
                    onChange={this.handleInputChange} /></td>
                <td>
                    <button onClick={() => this.addNewUser()}>Add</button>
                </td>
                <td></td>
            </tr>
        )
    }
}

export default NewUserRow