import React from 'react';
import PropTypes from 'prop-types';
import StaticRow from './StaticRow';
import EditRow from "./EditRow";

function MyTableBody(props) {
    const userLines = props.users.map(user => {
        if (user.editing) {
            return (
                <EditRow
                    user={user}
                    onClick={(type, id, data) => props.onClick(type, id, data)}
                />
            )
        } else {
            return (
                <StaticRow
                    user={user}
                    onClick={(type, id, data) => props.onClick(type, id, data)}
                />
            )
        }
    });
    return (
        <React.Fragment>{userLines}</React.Fragment>
    )
}

export default MyTableBody